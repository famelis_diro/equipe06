package com.implementation;

/**
 * G�rant unique de #GYM
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class Gerant extends Agent {
	Database db = Database.getInstance();

	/**
	 * Constructeur de la classe
	 * @param name: le nom du g�rant
	 */
	public Gerant(String name) {
		super(name);
	}

	/**
	 * Demande � la base de donn�e de produire le rapport de synth�se.
	 * Seul le g�rant peut en faire la demande autre que la g�n�ration automatique le vendredi � minuit
	 */
	public void demandWeekReport() {
		db.produceReport(); 
	}

}
