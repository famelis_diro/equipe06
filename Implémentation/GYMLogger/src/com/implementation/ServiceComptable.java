package com.implementation;

import java.util.ArrayList;


/**
 * G�re les comptes payables des clients � #GYM
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class ServiceComptable {
	private static final double monthlySubscriptionPrice = 39.99;


	/**
	 * 
	 * @return le prix d'inscription mensuel � #GYM
	 */
	public static double getSubscriptionPrice() {
		return monthlySubscriptionPrice;
	}


	/**
	 * 
	 * @param client: Le client � charger
	 * @param amount: Le montant charg�
	 * @return Le montant a �t� pay�, retourn true
	 */
	public boolean chargeCreditCard(Client client, double amount) {
		return true;
	}

	/**
	 * 
	 * @param clients: Liste de tous les clients dont le status doit �tre v�rifier aupr�s de RnB � chaque jour.
	 */
	public static void updateClientStatus(ArrayList<Client> clients) {
		// on simule la v�rification de paiement de chaque client (toujours valide)
		boolean paid;

		for (Client c :clients) {
			paid = RnB.clientPaid(c); //V�rification aupr�s de RnB si le client a pay� sa mensualit�.
			if (paid) {
				double balance = c.getBalance();
				c.addBalance(-balance); // on retire de la balance le montant d�
				c.setStatus("VALIDE");
			}
			else {
				c.setStatus("SUSPENDU");
			}
		}
	}
}
