package com.implementation;

import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * Agent (classe "main")
 * 
 * @author Jonathan Caspar
 * @author Jean-François Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class Agent {

	private String id;
	private String name;
	private Database db = Database.getInstance();
	private static Scanner scanner;
	public static SimpleDateFormat formatComplet = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public static String serviceHeader = String.format("%-8s | %-12s %-12s %-10s %-9s %-10s %-15s %-8s %-22s %-20s\n",
			"Numéro", "Début", "Fin", "Heure", "Jour", "Capacité", "Professionnel", "Frais", "Date de création",
			"Commentaire");
	public static String memberHeader = String.format("%-12s | %-20s %-20s %-12s %-20s %-30s %-8s %-12s", "Numéro",
			"Nom", "Date de naissance", "Téléphone", "Email", "Adresse", "Balance", "Statut");

	public Agent(String name) {
		this.name = name;
		this.id = GeneratorID.getInstance().generateMemberID();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// ------------------- Fonctions utilitaires --------------------------
	// Flush l'affichage précédent
	public static void flush() {
		for (int i = 0; i < 50; ++i)
			println("");
	}

	public static void println(Object str) {
		System.out.println(str);
	}

	public static void print(Object str) {
		System.out.print(str);
	}

	// Affiche une ligne de taille "size"
	public static void line(int size) {
		for (int i = 0; i < size; i++) {
			print("-");
		}
		println("");
	}

	// Met en "pause" le programme jusqu'à la pression de la touche Entrée
	public static void waitForEnter() {
		scanner = new Scanner(System.in);
		scanner.nextLine();
	}

	public static void wrongInput() {
		println("Commande non reconnue, veuillez ré-essayer.");
	}

	// Demande une donnée, la fonction renvoie l'input saisi
	public static String prompt(String msg) {
		if(msg != null) print(msg);
		scanner = new Scanner(System.in);
		return scanner.next();
	}

	public static String prompt() {
		return prompt(null);
	}

	// Demande une confirmation suite à un message donné (Oui ou Non)
	// Renvoie un boolean correspondant à la réponse, si mauvaise entrée : on redemande l'entrée
	public static boolean askConfirmation(String msg) {
		while(true) {
			switch(prompt(msg + " [O/N] ")){
			case "o":
			case "O":
				return true;
			case "n":
			case "N":
				return false;
			default :
				println("Commande non reconnue, veuillez ré-essayer.");
				break;	
			}
		}
	}

	// Affiche une énumeration à n chiffres pour un menu de n options
	public static void generateMenu(String... options) {
		for (int i = 0; i < options.length; i++) {
			println(i + 1 + ") " + options[i]);
		}
	}

	// Affiche un message en "flushant" le contenu précédent
	public static void showNotification(String msg) {
		flush();
		println(msg);
		waitForEnter();
	}


	// -------------------------------------------------------------

	// ----------------Fonctions de creation d'instances-------------

	protected void createClient() {
		flush();
		Client client = new Client();
		println("Entrez les informations du client : ");
		client.setName(prompt("Prénom : ") + " " + prompt("Nom : "));
		client.setBirthday(prompt("Date de naissance (JJ-MM-AAAA) : "));
		client.setPhone(prompt("Numéro de téléphone (XXX-XXX-XXXX) : "));
		client.setEmail(prompt("Adresse e-mail : "));
		client.setAdress(prompt("Adresse de résidence : "));
		client.setCreditCardInfo(prompt("Numéro de carte de crédit : "));

		if(askConfirmation("\nConfirmer l'inscription ?")) {

			db.addMember(client);
			println("Bienvenue " + client.getName() + " ! Votre numéro de membre unique est le " + client.getMemberID()
			+ ". Veuillez-le prendre en note.");
		}
	}

	protected void createProfessionnal() {
		flush();
		Professionnel professionnel = new Professionnel();
		println("Entrez les informations du professionel :");
		professionnel.setName( prompt("Prénom : ") + " " + prompt("Nom : "));
		professionnel.setBirthday(prompt("Date de naissance (JJ-MM-AAAA) : "));
		professionnel.setPhone(prompt("Numéro de téléphone (XXX-XXX-XXXX) : "));
		professionnel.setEmail(prompt("Adresse e-mail : "));
		professionnel.setAdress(prompt("Adresse de résidence : "));

		if (askConfirmation("\nConfirmer l'inscription ?")) {
			db.addMember(professionnel);

			println("Bienvenue " + professionnel.getName() + " ! Votre numéro de professionnel unique est le "
					+ professionnel.getMemberID() + ". Veuillez-le prendre en note.");
		}
	}

	protected void createService() {
		flush();
		Service service = new Service();
		println("Entrez les informations du service :");
		service.setStartDate(prompt("Date de début : "));
		service.setEndDate(prompt("Date de fin : "));
		service.setServiceHours(prompt("Heure du service : "));
		service.setRecurrenceDay(prompt("Horaire : "));
		service.setMaxCapacity(Integer.parseInt(prompt("Capacite maximale : ")));
		service.setProfessionalID(prompt("#Professionnel : "));
		service.setPrice(Double.parseDouble(prompt("Prix : ")));
		service.setComments(prompt("Commentaires : "));

		if (askConfirmation("\nConfirmer l'enregistrement du service ?")) {
			db.addService(service);
			showNotification("Le service n° " + service.getServiceID() + " a bien été enregistré.");
		}
	}

	protected void createEnregistrement(Service service, Membre m){
		Enregistrement e = new Enregistrement();
		println("Entrez les informations de l'enregistrement :");
		e.setServiceDate(prompt("Date de la séance (JJ-MM-AAAA) : "));
		e.setClientID(m.getMemberID());
		e.setProfessionalID(service.getProfessionalID());
		e.setServiceID(service.getServiceID());
		e.setComments(prompt("Commentaires : "));

		if (askConfirmation("\nConfirmer l'inscription ?")) {
			db.addInscription(e);
			showNotification("Inscription réussie !");
		}
	}

	// -------------------------------------------------------------

	// ----------------Fonctions gestion des comptes----------------

	// Demande un numéro de membre et retourne la référence à ce membre s'il existe
	protected Membre requestAccount() {
		while (true) {
			Membre membre = db.getMemberByID(prompt("Veuillez saisir le numéro du membre : "));
			if (membre != null) {
				return membre;
			} else {
				if (!askConfirmation("Membre introuvable. Voulez-vous réessayer ?")) {
					return null;
				}
			}
		}
	}

	protected void displayAccounts() {
		generateMenu("Afficher tout", "Rechercher par numéro de membre");
		switch(prompt("Que souhaitez-vous faire ? ")) {
		case "1": // Afficher tout
			String members = db.getMembersString();
			if(members.length() > 0) {
				println(memberHeader);
				line(140);
				println(members);
				waitForEnter(); // évite d'afficher le menu instantanément
			}
			else showNotification("Aucun membre trouvé dans la base de données.");
			break;

		case "2": // Recherche par numéro
			Membre membre = requestAccount();
			if(membre != null) {
				println(memberHeader);	// Affichage du header
				println(membre);	// Affichage du membre selectionné

				while(true) {
					switch(prompt("Souhaitez-vous modifier (1), supprimer (2) le compte ou revenir au menu principal (3) ?")){
					case "1":
						modifyAccount(membre);
						break;
					case "2":
						removeAccount(membre);
						break;
					case "3":
						return;
					default:
						wrongInput();
						break;
					}
				}
			}
			break;
		}
	}

	protected void modifyAccount(Membre m) {
		if (m != null) {
			while (true) {
				// Affichage des informations du membre
				String type = ((m instanceof Client)? "Client":"Professionnel");
				println("Informations du compte " + type + " numéro " + m.getMemberID() + " :");
				generateMenu("Nom et prénom : "  + m.getName(), 
						"Date de naissance : "   + m.getBirthday(),
						"Numéro de téléphone : " + m.getPhone(),
						"Adresse e-mail : "      + m.getEmail(),
						"Adresse de résidence : "+ m.getAdress());

				if(type.equals("Client")) {
					Client c = (Client) m;
					println("6) Numéro de carte de crédit : " + c.getCreditCardInfo());
				}

				// Choix du champ a modifier
				boolean done = false;

				while (!done) {
					switch (prompt("Quelle information souhaitez-vous modifier ?")) {
					case "1":
						m.setName( prompt("Prénom : ") + " " + prompt("Nom : ")); done = true;
						break;
					case "2":
						m.setBirthday(prompt("Date de naissance (JJ-MM-AAAA) : ")); done = true;
						break;
					case "3":
						m.setPhone(prompt("Numéro de téléphone (XXX-XXX-XXXX) : ")); done = true;
						break;
					case "4":
						m.setEmail(prompt("Adresse e-mail : ")); done = true;
						break;
					case "5":
						m.setAdress(prompt("Adresse de résidence : ")); done = true;
						break;
					case "6":
						if(type.equals("Client")) {
							((Client) m).setCreditCardInfo(prompt("Numéro de carte de crédit : ")); done = true;
						}
						break;
					default:
						wrongInput();
						break;
					}
				}
				// Demander à effectuer une autre modification
				if(!askConfirmation("Modification effectuée, voulez-vous effectuer une autre modification ?")) {
					return;
				}
			}
		}
	}

	protected void removeAccount(Membre membre) {
		if (membre != null) {
			String type = ((membre instanceof Client) ? "Client" : "Professionnel");
			if (askConfirmation(
					"/!\\ Voulez-vous vraiment supprimer le compte " + type + " de " + membre.getName() + " ?")) {
				db.removeMember(membre);
				println("Le compte " + type + " de " + membre.getName() + " a bien été supprimé.");
			}
		}
	}

	// -------------------------------------------------------------

	// --------------Fonctions gestion des services-----------------

	// Demande un numéro de service et retourne la référence à ce service s'il existe
	protected Service requestService() {
		while (true) {
			Service service = db.getServiceByID(prompt("Veuillez saisir le numéro du service : "));
			if (service != null) {
				return service;
			} else {
				if (!askConfirmation("Service introuvable. Voulez-vous réessayer ?")) {
					return null;
				}
			}
		}
	}

	protected void displayServices() {
		flush();
		print(serviceHeader);
		line(140);
		println(db.getServicesString());
	}

	protected void modifyService(Service s) {
		if (s != null) {
			while (true) {
				// Affichage des informations du membre
				println("Informations du service " + s.getServiceID());
				generateMenu("Date de debut : " + s.getStartDate(), "Date de fin : " + s.getEndDate(),
						"Heure : " + s.getServiceHours(), "Horaire : " + s.getRecurrenceDay(),
						"Capacite maximale : " + s.getMaxCapacity(), "ID du professionnel : " + s.getProfessionalID(),
						"ID du service : " + s.getServiceID(), "Prix : " + s.getPrice(),
						"Commentaires : " + s.getComments());

				// Choix du champ à modifier
				boolean done = false;
				while (!done) {
					switch (prompt("Quelle information souhaitez-vous modifier ?")) {
					case "1":
						s.setStartDate(prompt("Entrez la date de debut de service (JJ-MM-AAAA : "));
						done = true;
						break;
					case "2":
						s.setEndDate(prompt("Entrez la date de fin du service (JJ-MM-AAAA) : "));
						done = true;
						break;
					case "3":
						s.setServiceHours(prompt("Entrez l'heure du service : "));
						done = true;
						break;
					case "4":
						s.setRecurrenceDay(prompt("Entrez l'horaire du service : "));
						done = true;
						break;
					case "5":
						s.setMaxCapacity(Integer.parseInt(prompt("Entrez la capacite maximale : ")));
						done = true;
						break;
					case "6":
						s.setProfessionalID(prompt("Entrez le #Professionnel : "));
						done = true;
						break;
					case "7":
						s.setServiceID(prompt("Entrez le #Service : "));
						done = true;
						break;
					case "8":
						s.setComments(prompt("Commentaires : "));
						done = true;
						break;
					default:
						wrongInput();
						break;
					}
				}

				// Demander a effectuer une autre modification
				if (!askConfirmation("Modification effectee, voulez-vous effectuer une autre modification ?"))
					return;
			}
		}

	}

	protected void removeService(Service s) {
		if (s != null) {
			if (askConfirmation(
					"/!\\ Voulez-vous vraiment supprimer le service ")) {
				db.removeService(s);
				println("Le service a ete supprime ");
			}
		}
	}

	// -------------------------------------------------------------

	// Vérifie l'autorisation d'un membre et affiche son statut
	protected void accessGYM() {
		Membre membre = requestAccount();
		if (membre != null) {
			if (membre instanceof Client) {
				Client c = (Client) membre;

				if (c.isValid()) {
					showNotification("Validé");
				} else {
					showNotification(" Membre suspendu : des frais de " + c.getBalance() + " $ sont dus.");
				}
			} else
				showNotification("Ce numéro de membre correspondant é celui d'un professionnel.");
		}
	}

	// Inscrit un membre à un service
	protected void registrationToService() {
		Membre membre 	= requestAccount();

		if (membre != null) { // Le numéro de membre et de service demandé sont valides
			if(membre instanceof Client) {

				Service service = requestService();
				if(service != null) {

					if(askConfirmation("Le prix de la séance est de " + String.valueOf(service.getPrice()) + " $."
							+ "\nCe montant sera ajouté à la balance du client " + membre.getName() +".\nContinuer ?")) {
						createEnregistrement(service, membre);	  // On crée un enregistrement avec les infos du service et du client concerné
						((Client) membre).addBalance(service.getPrice()); // On ajoute le montant à la balance du client
						service.decrementCapacity();   					  // On actualise la capacité du service
					}
				}
			}
			else showNotification("Impossible d'inscrire un professionnel à un service.");
		} 
	}

	protected void confirmPresence() {
		Membre membre = requestAccount();

		if (membre != null) { // Le numéro de membre et de service demandé sont valides
			if(membre instanceof Client) {

				Enregistrement inscription = db.getInscriptionOfMember(membre.getMemberID());
				if (inscription != null) { // Une inscription du membre est trouvée

					if(askConfirmation("Inscription de " + membre.getName() + " trouvé pour la séance donnée le " + inscription.getServiceDate() + ". Confirmer la présence ?")) {
						db.addRenderedService(inscription); // on stocke l'inscription dans les services rendus
						db.removeInscription(inscription);  // on supprime l'inscription de la liste des inscriptions en attente de confirmation
						println("Confirmation réussie !");
					}	
				}
				else showNotification("Aucune inscription à une séance au nom de " + membre.getName() + " n'a été trouvée.");
			}
		}
	}

	protected void checkRegisteredClients() {
		Membre pro 	= requestAccount();

		if (pro != null) { // Le numéro de membre et de service demandé sont valides
			if (pro instanceof Professionnel) {

				println("Membres inscrits aux services donnés par le professionnel " + pro.getName() + " :");

				// Pour chaque service du professionnel, on affiche les membres inscrits à celui ci
				for (Service service : ((Professionnel) pro).getServices()) {
					println("Service : " + service);

					for (Client client : db.getMembersRegisteredToService(service.getServiceID())) {
						println("----> " + client.toString());
					}
				}
			} else showNotification("Impossible de consulter les services donnés par un client.");
		} 
	}
	
	// Menu de gestion des comptes
	protected void accountManagement() {
		flush();
		generateMenu("Afficher la liste des comptes", "Créer un compte client", "Créer un compte professionel", "Modifier un compte", "Supprimer un compte");
		switch(prompt()) {
		case "1":	// Afficher la liste des comptes"
			flush();
			displayAccounts();
			break;
		case "2":	// Créer un compte client
			flush();
			createClient();
			break;
		case "3":	// Créer un compte professionel
			flush();
			createProfessionnal();
			break;
		case "4":	// Modifier un compte
			flush();
			modifyAccount(requestAccount());
			break;
		case "5":	// Supprimer un compte
			flush();
			removeAccount(requestAccount());
			break;
		default:
			wrongInput();
			break;
		}
	}
	
	// Menu de gestion de services
	protected void serviceManagement() {
		flush();
		generateMenu("Afficher la liste des services", "Créer un service", "Modifier un service", "Supprimer un service");
		switch(prompt()) {
		case "1":	// Afficher la liste des services
			displayServices();
			break;
		case "2":	// Créer un service
			createService();
			break;
		case "3":	// Modifier un service
			modifyService(requestService());
			break;
		case "4":	// Supprimer un service
			removeService(requestService());
			break;
		default:
			wrongInput();
			break;
		}
	}
	/**
	 *	Menu principal
	 */
	public static void main(String[] args){
		//Agent gerant = new Gerant("Bob");
		Agent agent = new Agent("Smith");
		boolean logged = true;

		while (logged) {
			generateMenu("Gestion des comptes", "Gestion des services", "Autoriser accès au GYM", "Accéder au répertoire des services", 
					"Inscrire un membre à une séance", "Confirmer la presence a une séance", "Consulter les membres inscrits", "Produire un rapport de synthèse", "Se déconnecter");
			switch (prompt()) { // Menu principal

			case "1": // Gestion des comptes
				agent.accountManagement();
				break;

			case "2": // Gestion des services
				agent.serviceManagement();
				break;	

			case "3": // Autoriser accès au GYM
				agent.accessGYM();
				break;

			case "4": // Accéder au répertoire des services
				agent.displayServices();
				break;

			case "5": // Inscrire un membre à une seance
				agent.registrationToService();
				break;

			case "6": // Confirmer la présence à une séance
				agent.confirmPresence();
				break;

			case "7": // Consulter les membres inscrits
				agent.checkRegisteredClients();
				break;

			case "8": // Produire rapport synthese
				if (agent.db.getAgentByID(prompt("#Gerant :"))!= null) {
					agent.db.produceReport();
				}
				else println("#Gerant erroné\n");
				break;

			case "9": // Déconnecter
				logged = false;
				break;

			default:
				wrongInput();
				break;
			}
		}
	}
}
