package com.implementation;

/**
 * G�n�rateur de numro d'identit� pour membre et service. Permet de garder une trace des num�ro donn�e.
 * G�n�re les num�ro dans l'ordre croissant.
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */

public class GeneratorID {
	private static GeneratorID INSTANCE = null;
	private long lastMemberID =  200009000;
	private long lastServiceID = 4009000;

	/**
	 * Constructeur de la classe
	 */
	private GeneratorID() {}

	/**
	 * Point d'acc�s pour l'instance unique du g�n�rateur
	 * @return une instance du g�n�rateur de num�ro d'identit�
	 */
	public static synchronized GeneratorID getInstance() {           
		if (INSTANCE == null) {   
			INSTANCE = new GeneratorID(); 
		}
		return INSTANCE;
	}

	/**
	 * 
	 * @return une cha�ne de caract�re prochain num�ro de membre attribu�
	 */
	public String generateMemberID() {
		return String.valueOf(lastMemberID ++);
	}

	/**
	 * 
	 * @return une cha�ne de caract�re prochain num�ro de service attribu�
	 */
	public String generateServiceID() {
		return String.valueOf(lastServiceID ++);
	}
}
