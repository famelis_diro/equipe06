package com.implementation;

import java.util.Date;

/**
 * G�re les services offerts par les professionnel � #GYM
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 * Constructeur de la classe
 * @param startDate : D�but du service
 * @param endDate: fin du service
 * @param serviceHours: heure � laquelle le service se donne
 * @param recurrenceDay: journ�e de la semaine o� le service est offert
 * @param maxCapacity: capacit� d'une s�ance du service
 * @param professionalID: ID du professionnel offrant le service
 * @param price: prix du service
 * @param comments: commentaires facultatifs
 */
public class Service {
	private String creationDate;
	private String startDate;
	private String endDate;
	private String serviceHours;
	private String recurrenceDay;
	private int maxCapacity;
	private String professionalID;
	private String serviceID;
	private double price;
	private String comments;

	public Service(String startDate, String endDate, String serviceHours, String recurrenceDay,
			int maxCapacity, String professionalID, double price, String comments) {
		this.creationDate 	= Agent.simpleDateFormat.format(new Date());
		this.startDate 		= startDate;
		this.endDate 		= endDate;
		this.serviceHours	= serviceHours;
		this.recurrenceDay 	= recurrenceDay;
		this.maxCapacity 	= maxCapacity;
		this.professionalID = professionalID;
		this.serviceID		= GeneratorID.getInstance().generateServiceID();
		this.price 			= price;
		this.comments		= comments;
	}

	/**
	 * G�n�re automatiquement la date de cr�ation du service (aujourd'hui), et un num�ro unique
	 */
	public Service() {
		this.creationDate = Agent.simpleDateFormat.format(new Date());
		this.serviceID 	  = GeneratorID.getInstance().generateServiceID();
	}

	public String getCreationDate() {
		return creationDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getServiceHours() {
		return serviceHours;
	}

	public void setServiceHours(String serviceHours) {
		this.serviceHours = serviceHours;
	}

	public String getRecurrenceDay() {
		return recurrenceDay;
	}

	public void setRecurrenceDay(String recurrenceDay) {
		this.recurrenceDay = recurrenceDay;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}
	
	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public String getProfessionalID() {
		return professionalID;
	}

	public void setProfessionalID(String professionalID) {
		this.professionalID = professionalID;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * �criture 'propre' du service et de ses d�tails
	 */
	@Override
	public String toString() {
		return String.format("%-8s | %-12s %-12s %-10s %-9s %-10s %-15s %-8s %-22s %-20s\n", 
				String.valueOf(serviceID),
				String.valueOf(startDate), 
				String.valueOf(endDate), 
				String.valueOf(serviceHours), 
				String.valueOf(recurrenceDay), 
				String.valueOf(maxCapacity), 
				String.valueOf(professionalID), 
				String.valueOf(price) + " $", 
				String.valueOf(creationDate),
				String.valueOf(comments));
	}

	/**
	 * Met � jour le nombre de place disponible dans une s�ance du service quand le client est confirm�
	 */
	public void decrementCapacity() {
		if(maxCapacity > 0) maxCapacity--;
	}
}
