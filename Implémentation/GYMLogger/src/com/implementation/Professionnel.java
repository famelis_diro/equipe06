package com.implementation;

import java.util.ArrayList;

public class Professionnel extends Membre {

	private ArrayList<Service> services;

	public Professionnel(String name, String birthday, String phone, String email, String adress) {
		super(name, birthday, phone, email, adress);
		this.services = new ArrayList<Service>();
	}

	public Professionnel() {
		super();
		this.services = new ArrayList<Service>();
	}

	public ArrayList<Service> getServices(){
		return this.services;
	}

	public boolean addService(Service service) {
		if(!services.contains(service)) {
			services.add(service);
			return true;
		}
		return false;
	}

	public void removeService(Service service) {
		if(services.contains(service)) {
			services.remove(service);
		}
	}
}
