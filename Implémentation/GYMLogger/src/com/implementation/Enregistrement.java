package com.implementation;

import java.util.Date;

public class Enregistrement {
	private String creationDate;
	private String serviceDate;
	private String professionalID;
	private String clientID;
	private String serviceID;
	private String agentID;
	private String comments;


	// Constructeur pour enregistrement d'inscription � un service futur
	public Enregistrement(String serviceDate, String professionalID, String clientID, String serviceID, String comments) {
		this.creationDate = Agent.formatComplet.format(new Date());
		this.serviceDate = serviceDate;
		this.professionalID = professionalID;
		this.clientID = clientID;
		this.serviceID = serviceID;
		this.comments = comments;

	}

	// Constructeur pour enregistrement de service rendu
	public Enregistrement(String professionalID, String clientID, String serviceID, String comments) {
		this.creationDate = Agent.formatComplet.format(new Date());;
		this.professionalID = professionalID;
		this.clientID = clientID;
		this.serviceID = serviceID;
		this.comments = comments;
	}

	public Enregistrement() {
		super();
	}


	public String getCreationDate() { return creationDate; }

	public String getServiceDate() { return serviceDate; }

	public String getProfessionalID() { return professionalID; }

	public String getAgentID() { return agentID; }

	public String getClientID() { return clientID; }

	public String getServiceID() { return serviceID; }

	public String getComments() { return comments; }

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public void setProfessionalID(String professionalID) {
		this.professionalID = professionalID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
