package com.implementation;

/**
 * �mulation d'une horloge automatique.
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class Horloge {

	private String dayOfWeek = "Friday";
	private int hourOfDay = 0;
	private int minuteOfHour = 0;
	private int secondOfMinute = 0;

	/**
	 * Constructeur de la base
	 */
	private Horloge() {}

	/**
	 * V�rifie s'il est vendredi minuit.
	 * @return true s'il est vendredi minuit, sinon retourne false.
	 */
	public boolean isFridayMidnight() {
		boolean fridayMidnight = false;
		if(dayOfWeek == "Friday" && hourOfDay == 0 && minuteOfHour == 0 && secondOfMinute == 0 )
			fridayMidnight = true;
		return fridayMidnight;
	}

}
