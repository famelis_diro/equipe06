package com.implementation;

/**
 * Repr�sentation des membres de #GYM (clients et professionnels)
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public abstract class Membre {
	protected String name;
	protected String birthday;
	protected String phone;
	protected String email;
	protected String adress;
	protected String memberID;

	public Membre (String name, String birthday, String phone, String email, String adress) {
		this.name = name;
		this.birthday = birthday;
		this.phone = phone;
		this.email = email;
		this.adress = adress;
		this.memberID = GeneratorID.getInstance().generateMemberID();
	}

	public Membre () {
		this.memberID = GeneratorID.getInstance().generateMemberID();
	}

	public String getName() { return name; }

	public String getBirthday() { return birthday; }

	public String getPhone() { return phone; }

	public String getEmail() { return email; }

	public String getAdress() { return adress; }

	public String getMemberID() { return memberID; }

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthday(String date) {
		this.birthday = date;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	/**
	 * @return Un string des membres en �criture propre
	 */
	@Override
	public String toString() {
		return String.format("%-12s | %-20s %-20s %-12s %-20s %-30s", memberID, name, birthday, phone, email, adress);
	}
}
