package com.implementation;

/**
 * Simule le service comptable RnB
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class RnB {

	/**
	 * �mulateur de la r�ponse envoy�e par RnB si le client a pay� ses frais.
	 * @param client: client dont on v�rifie si la mensualit� a �t� pay�
	 * @return Un boolean: true = le client a pay�; false: le client est en dette
	 */
	public static boolean clientPaid(Client client) {
		return true;
	}

}
