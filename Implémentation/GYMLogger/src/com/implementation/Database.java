package com.implementation;

import java.util.ArrayList;

/**
 * Base de données (singleton) contenant les membres, les services, les agents, les inscription et les confirmations de présence
 * 
 * @author Jonathan Caspar
 * @author Jean-François Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class Database {

	private static Database INSTANCE = null;
	private ArrayList<Membre> members;
	private ArrayList<Service> services;
	private ArrayList<Enregistrement> inscriptions;
	private ArrayList<Enregistrement> renderedServices;
	private ArrayList<Agent> agents;

	/**
	 * Constructeur de la base
	 */
	private Database() {
		this.members = new ArrayList<Membre>();
		this.services = new ArrayList<Service>();
		this.inscriptions = new ArrayList<Enregistrement>();
		this.renderedServices = new ArrayList<Enregistrement>();
		this.agents = new ArrayList<Agent>();
	}

	// Point d'accés pour l'instance unique de la Database
	public static synchronized Database getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Database();
		}
		return INSTANCE;
	}

	/**
	 * 
	 * @return la liste de services
	 */
	public ArrayList<Service> getServices() {
		return services;
	}

	/**
	 * @return un string contenant les informations de tous les services offert
	 */
	public String getServicesString() {
		StringBuilder sb = new StringBuilder();
		for (Service service : services) {
			sb.append(service.toString());
		}
		return sb.toString();
	}

	/**
	 * @return la liste de tous les membres de #GYM
	 */
	public ArrayList<Membre> getMembers() {
		return members;
	}

	/**
	 * @return un string contenant les informations de tous les membres
	 */
	public String getMembersString() {
		StringBuilder sb = new StringBuilder();
		for (Membre m : members) {
			if (m instanceof Client)
				sb.append(((Client) m).toString() + "\n");
			if (m instanceof Professionnel)
				sb.append(((Professionnel) m).toString() + "\n");
		}
		return sb.toString();
	}

	public ArrayList<Enregistrement> getInscriptions() {
		return inscriptions;
	}

	public ArrayList<Enregistrement> getRenderedServices() {
		return renderedServices;
	}

	public ArrayList<Agent> getAgents() {
		return agents;
	}

	public void addAgent(Agent agent) {
		agents.add(agent);
	}

	public void addMember(Membre member) {
		members.add(member);
	}

	public void addService(Service service) {
		services.add(service);

		// On ajoute ce service dans l'attributs services du professionnel concerné
		Professionnel pro = getProfessionalByID(service.getProfessionalID());
		if (pro != null) { // le professionel existe
			pro.addService(service);
		}
	}

	public void addInscription(Enregistrement data) {
		inscriptions.add(data);
	}

	public void addRenderedService(Enregistrement data) {
		renderedServices.add(data);
	}

	public boolean removeMember(Membre member) {
		if (members.contains(member)) {
			members.remove(member);
			return true;
		}
		return false;
	}

	public boolean removeService(Service service) {
		if (services.contains(service)) {
			services.remove(service);
			// On supprime ce service dans l'attributs services du professionnel concerné
			Professionnel pro = getProfessionalByID(service.getProfessionalID());
			if (pro != null) { // le professionel existe
				pro.removeService(service);
			}
			return true;
		}
		return false;

	}

	public boolean removeInscription(Enregistrement data) {
		if (inscriptions.contains(data)) {
			inscriptions.remove(data);
			return true;
		}
		return false;
	}

	public boolean removeRenderedService(Enregistrement data) {
		if (renderedServices.contains(data)) {
			renderedServices.remove(data);
			return true;
		}
		return false;
	}

	// Retourne la référence d'un objet Client ayant un ID spécifié
	public Client getClientByID(String ID) {
		if (ID.length() == 9) {
			for (Membre m : members) {
				if ((m instanceof Client) && (m.getMemberID().equals(ID))) {
					return (Client) m;
				}
			}
		}
		return null;
	}

	// Retourne la référence d'un objet Agent ayant un ID spécifié
	public Agent getAgentByID(String ID) {
		if (ID.length() == 9) {
			for (Agent a : agents) {
				if ((a instanceof Agent) && (a.getId().equals(ID))) {
					return (Agent) a;
				}
			}
		}
		return null;
	}

	// Retourne la reference d'un objet Gerant ayant un id specifie
	public Gerant getGerantByID(String ID) {
		if (ID.length() == 9) {
			for (Agent a : agents) {
				if ((a instanceof Gerant) && (a.getId().equals(ID))) {
					return (Gerant) a;
				}
			}
		}
		return null;
	}

	//Retourne la référence d'un objet Professionel ayant un ID spécifié
	public Professionnel getProfessionalByID(Object object) {
		if (((String) object).length() == 9) {
			for (Membre m : members) {
				if ((m instanceof Professionnel) && (m.getMemberID().equals(object))) {
					return (Professionnel) m;
				}
			}
		}
		return null;
	}

	// Retourne la référence d'un objet Service ayant un ID spécifié
	public Service getServiceByID(String ID) {
		if (ID.length() == 7) {
			for (Service s : services) {
				if (s.getServiceID().equals(ID)) {
					return s;
				}
			}
		}
		return null;
	}


	//Retourne la reference de l'enregistrement stockant l'inscription d'un membre donné
	public Enregistrement getInscriptionOfMember(String memberID) {
		for (Enregistrement e : inscriptions) {
			if (e.getClientID().equals(memberID))
				return e;
		}
		return null;
	}

	//Retourne la reference de l'enregistrement stockant l'inscription d'un membre donné
	public ArrayList<Client> getMembersRegisteredToService(String serviceID) {
		ArrayList<Client> clientsInscrits = new ArrayList<Client>();

		for (Enregistrement e : inscriptions) {
			if (e.getServiceID().equals(serviceID)) {
				Client client = getClientByID(e.getClientID()); // On récupère la référence vers le client s'étant inscrit au service donné
				clientsInscrits.add(client);
			}
		}
		return clientsInscrits;
	}

	//Retourne la reference d'un membre avec l'id specifie
	public Membre getMemberByID(String ID) {
		if (ID.length() == 9) {
			for (Membre m : members) {
				if (m.getMemberID().equals(ID)) {
					return m;
				}
			}
		}
		return null;
	}

	public void procedureComptable(Horloge h) {
		if (h.isFridayMidnight()) {
			produceReport();
			produceTEF();
		}
	}

	//Imprime à la console le rapport de tous les professionnels, 
	public void produceReport() {
		Professionnel p;

		Agent.println("**Rapport de transfert de fond**");
		for (Membre m: members) {
			if (m instanceof Professionnel) {
				p = (Professionnel)m;
				int frais = 0;
				Agent.println("Professionnel: " + p.getName() + "Nombre de services: " + p.getServices().size());
				for (Service s: p.getServices()) {
					int numInscription = 0;
					for (Enregistrement e: renderedServices) {
						if (s.getServiceID() == e.getServiceID())
							numInscription++;
					}
					frais += numInscription * s.getPrice();
				}
				System.out.println("Frais a payer au professionnel : " + frais);
			}
		}
		System.out.println("**Rapport envoyé**");
	}

	private void produceTEF() {
		Professionnel p;

		Agent.println("**Rapport de transfert de fond**");
		for (Membre m: members) {
			if (m instanceof Professionnel) {
				p = (Professionnel) m;
				System.out.println("Professionnel: " + p.getName() + ", Numéro: " + p.getMemberID());
				System.out.println("Liste de services:");
				for (Service s: p.getServices()) {
					int numInscription = 0;
					for (Enregistrement e: renderedServices) {
						if (s.getServiceID() == e.getServiceID())
							numInscription++;
					}
					System.out.println(s.getServiceID() + " Nombre d'inscription: " + numInscription);
				}

			}
		}
		System.out.println("**Rapport envoyé**");
	}

}
