package com.implementation;

/**
 * Repr�sentation des clients de #GYM
 * 
 * @author Jonathan Caspar
 * @author Jean-Fran�cois Blanchette
 * @author Alexandre Dufour
 * 
 * @version 1.0
 *
 */
public class Client extends Membre {

	private String status;
	private double balance;
	private String numeroCarteCredit;

	/**
	 * Constructeur de la classe
	 * 
	 * @param name
	 * @param birthday
	 * @param phone
	 * @param email
	 * @param adress
	 * @param creditCard
	 */
	public Client(String name, String birthday, String phone, String email, String adress, String creditCard) {
		super(name, birthday, phone, email, adress);
		this.status = "VALIDE"; // statut par d�faut Invalide
		this.balance = ServiceComptable.getSubscriptionPrice(); // facturation initiale
		this.numeroCarteCredit = creditCard;
	}

	public Client() {
		super();
		this.status = "VALIDE"; // statut par d�faut Invalide
		this.balance = ServiceComptable.getSubscriptionPrice(); // facturation initiale
	}


	public String getStatus() {
		return status;
	}


	public String getCreditCardInfo() {
		return numeroCarteCredit;
	}

	public double getBalance() {
		return balance;
	}

	/**
	 * 
	 * @return un bool�an qui v�rifie si le status est valide
	 */
	public boolean isValid() {
		return status.equals("VALIDE") ? true : false;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Ajoute ou retire un montant sp�fici� � la balance du client
	 * @param amount
	 * @return nouvelle balance
	 */
	public double addBalance(double amount) {
		this.balance += amount;
		return this.balance;
	}

	public void setCreditCardInfo(String numeroCarte) {
		this.numeroCarteCredit = numeroCarte;
	}

	/**
	 * @return Un string des clients en �criture propre
	 */
	@Override
	public String toString() {
		return String.format("%-12s | %-20s %-20s %-12s %-20s %-30s %-8s$ %-12s\"", memberID, name, birthday, phone, email, adress, balance, status);
	}
}
