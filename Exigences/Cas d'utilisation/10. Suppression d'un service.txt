Cas d�utilisation : Suppression d'un service
But : Un professionnel souhaite supprimer un de ses services
Pr�condition : Le professionnel a cr�e au moins un service
Postcondition : Un service est effacer du centre de donn�es
Acteurs : Agent (principal), Professionnel (secondaire)
Sc�nario principal :
    1. Le syst�me demande un num�ro de membre � identifier que l�agent demande au professionnel
    2. Le syst�me appelle S�identifier
    3. Le syst�me affiche la liste des services enregistr�s au nom du professionnel
    4. L'agent supprime le service indiqu� par le professionnel
    5. Le syst�me supprime le service dans la base de donn�es
Sc�narios alternatifs :
    3a : La r�ponse de l�appel S�identifier est n�gative:
    3a.1 : le syst�me affiche sur un �cran la raison du refus (Num�ro invalide, Membre suspendu, �)
        3a.2 : Retour au point 1