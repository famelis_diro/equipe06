Cas d'utilisation : Consulter les inscription � une s�ance
But : consulter la liste des membres qui ont confirm� leur pr�sence � une s�ance
Pr�condition : Le professionnel a un compte
Postcondition :
Acteurs : Agent (principal), Professionnel (secondaire)
Sc�nario principal:
    1. Le syst�me demande un num�ro de membre � identifier que l�agent demande au professionnel
    2. Le syst�me appelle S�identifier
    3. Le syst�me affiche la liste des services enregistr�s au nom du professionnel
        4. L'agent s�lectionne le service que le professionnel choisit
        5. Le syst�me affiche la liste des membres qui ont confirm� leur pr�sence � la prochaine s�ance
Sc�narios alternatifs : 
    3a : La r�ponse de l�appel S�identifier est n�gative:
    3a.1 : Le syst�me affiche sur un �cran la raison du refus (Num�ro invalide, Membre suspendu, �)
        3a.2 : Reprend au point 1