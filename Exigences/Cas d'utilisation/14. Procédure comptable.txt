Cas d�utilisation : Proc�dure comptable 
But : Envoyer automatiquement aux services comptable RnB le fichier TEF et envoyer au g�rant un rapport de synth�se
Pr�condition: Etre Vendredi � minuit
Postcondition: Proc�dure comptable inactive jusqu'� la semaine suivante
Acteurs : Horloge (principal)
Sc�nario principal :
	1. Appel de Rapport TEF
	2. Appel de Rapport de synth�se
	3. Retour � l'�tat d'origine du syst�me