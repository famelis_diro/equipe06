Cas d'utilisation: Cr�er un compte - Professionel
But: Cr�er un compte � un nouveau professionel avec ses informations personnelles et lui 
     fournit un num�ro de membre al�atoire disponible.
Postcondition : Un compte Professionel � �t� cr��
Acteur: Agent (principal), Professionel (secondaire)

Sc�nario principal :
    1. L'agent ouvre le logiciel de Centre de Donn�es.
    2. L'agent s�lectionne l'option "Cr�er un compte".
    3. L'agent s�lectionne le profil du membre : "Professionel"
    4. L'agent remplie les champs libres dans le logiciel avec les informations du nouveau membre.
        4.1. L'agent entre le pr�nom.
        4.2. L'agent entre le nom de famille.
        4.3. L'agent entre la date de naissance.
        4.4. L'agent entre le num�ro de t�l�phone.
        4.5. L'agent entre l'adresse courriel.
        4.6. L'agent entre l'adresse civique.
        4.7. L'agent entre les informations de compte (pour la paie).
    5. L'agent presse le bouton "Confirmer l'inscription".
    6. Le logiciel affiche � l'�cran un "Num�ro de membre" al�atoire et libre.
    7. L'agent presse le bouton "Inscrire".
    8. L'agent ferme le logiciel de Centre de Donn�es.


Sc�narios alternatifs:
    4a : Le professionnel n'a pas toutes ses informations personnelles.
            4a.1 : Si le membre n'a pas son num�ro de t�l�phone, son adresse courriel ou adresse civique.
                4a.1.1 : Compl�ter les champs dont les informations sont disponibles.
                4a.1.2 : Cliquer sur "Remplir les champs manquants ult�rieurement".
                4a.1.3 : Continuer au point 5.