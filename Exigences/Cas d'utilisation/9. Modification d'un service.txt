Cas d�utilisation : Modification d'un service
But : Un professionnel souhaite modifier les informations d'un de ses services
Pr�condition : Le professionnel a cr�e au moins un service
Postcondition : Les informations du service sont modifi�es
Acteurs : Agent (principal), Professionnel (secondaire)
Sc�nario principal :
    1. Le syst�me demande un num�ro de membre � identifier que l�agent demande au professionnel
    2. Le syst�me appelle S�identifier
    3. Le syst�me affiche la liste des services enregistr�s au nom du professionnel
    4. L'agent fournit au syst�me les nouvelles informations du service que le professionnel choisit
    5. Le syst�me met � jour les informations du service s�lectionn�
Sc�narios alternatifs :
    3a : La r�ponse de l�appel S�identifier est n�gative:
    3a.1 : le syst�me affiche sur un �cran la raison du refus (Num�ro invalide, Membre suspendu, �)
        3a.2 : Retour au point 1

    5a : Certaines informations fournies sont invalides (champ vide, format incorrect, etc.)
    5a.1 : Retour au point 4