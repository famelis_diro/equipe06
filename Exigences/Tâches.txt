﻿Répartition des tâches:
DM1:

Cas d'utilisation:

	Créer un compte --Alex	
	S'identifier --JF
	accès à #gym --Jo
	créer un service --JF
	Consulter le répertoire des services --JF
	s'inscrire à une séance --Jo
	confirmer présence --Jo
	consulter les inscriptions --JF
	Modification d'un service -- Jo
	Suppression d'un service -- Jo
	Modification des informations d'un compte -- Alex
	Suppression d'un compte -- Alex
	
Glossaire: Tous (brain-storming)

Liste des risques: Tous (brain-storming)

Liste des besoins et contraintes non-fonctionnels: Tous (brain-storming)




DM2:IFT 2255 TP2
Tache 1 : Exigences :
CU : procédure comptable génération fichier TEF
CU : séparer compte professionnel et utilisateur
Génération rapport de synthèse
Vérification de status compte

Modification (« créer service » ) : frais de service dans l’enregistrement

Tache 2 : Analyse
1)    MaJ des status (chaque soir à 21h)
2)    Traitement de paie des professionnels (TEF) chaque vendredi à minuit
3)    Rapport de synthèse envoyé au gérant
•    Dans notre cas, Centre de données = structure de données interne (Classe Database)
•    Ordinateur connecté à Internet
•    Interaction avec les autres systèmes via internet

•     Manuel : S’identifier, vérifier paiement/status, Comment créer/modifier/supprimer un compte professionnel + membre, Comment inscrire à un service, Comment générer rapport de synthèse, comment produire un TEF, Comment confirmer présence,
				Accéder au gym, créer/modifier/supprimer un service et répertoire, consulter les inscriptions,
Tache 3 :
Début du diagramme = Identification dans le système
Autres diagrammes : membre, service, inscription séance, confirmer présence, procédure comptable



Répartition :
•    Jonathan :
o    Tache 1 : Scénario procédure comptable + compte utilisateur
o    Tache 2 : Pour le manuel: S’identifier, vérifier paiement/status, Comment créer/modifier/supprimer un compte professionnel + membre
o    Tache 3 : Identification dans le système, ajouter/supprimer/mettre à jour un membre
o    Tache 4 :

•    JF :
o    Tache 1 : Génération rapport de synthèse + Vérification de status compte
o    Tache 2 : Pour le manuel: Comment s'inscrire à un service, Comment générer rapport de synthèse, comment produire un TEF
o    Tache 3 : ajouter/supprimer/mettre à jour un service, inscription séance
o    Tache 4 :

•    Alex :
o    Tache 1 : MAJ Diagramme CU
o    Tache 2 : Pour le manuel: Comment confirmer présence, Accéder au gym, créer/modifier/supprimer un service et répertoire, consulter les inscriptions
o    Tache 3 : Confirmer présence, procédure comptable
o    Tache 4 :
